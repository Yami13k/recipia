from django.contrib.auth import get_user_model


def sample_user(email="test@gmail.com", password="test pass"):
    """Create a sample user"""
    return get_user_model().objects.create_user(email, password)